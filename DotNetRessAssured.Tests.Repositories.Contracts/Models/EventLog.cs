﻿using System;

namespace DotNetRessAssured.Tests.Repositories.Contracts.Models
{
    public class EventLog
    {
        public Guid TransactionId { get; set; }
        public string Process { get; set; }
        public string Module { get; set; }
        public string Message { get; set; }
        public string Timestamp { get; set; }
        public string Status { get; set; }
        public string Detail { get; set; }
        public string AditionalData { get; set; }
        public string NameProvider { get; set; }
        public string IdResponseProvider { get; set; }
    }
}
