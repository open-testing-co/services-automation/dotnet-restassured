﻿namespace DotNetRessAssured.Tests.Repositories.Contracts.Models
{
    public class TrackingStatus
    {
        public string timestamp { get; set; }
        public string detail { get; set; }
        public string status { get; set; }
    }
}
