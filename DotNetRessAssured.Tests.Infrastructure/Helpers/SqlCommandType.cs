﻿namespace DotNetRessAssured.Tests.Infrastructure.Helpers
{
    public enum SqlCommandType
    {
        Sql,
        StoredProcedure
    }
}
