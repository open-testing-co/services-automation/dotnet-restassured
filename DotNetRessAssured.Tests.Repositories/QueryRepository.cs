﻿using System;

namespace DotNetRessAssured.Tests.Repositories
{
    public class QueryRepository
    {
        public string Get(string key)
        {
            string result = QueryResources.ResourceManager.GetString(key);
            if (string.IsNullOrEmpty(result))
                throw new Exception("Resoruce Not Found");

            return result;
        }
    }
}
