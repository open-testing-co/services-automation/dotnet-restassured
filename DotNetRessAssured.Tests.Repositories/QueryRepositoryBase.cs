﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DotNetRessAssured.Tests.Infrastructure.Helpers;

namespace DotNetRessAssured.Tests.Repositories
{
    public class QueryRepositoryBase
    {
        readonly string conectionString = "Server=tcp:server.database.windows.net,1433;Initial Catalog=catalog-db;Persist Security Info=False;User ID=username;Password=Passw0rd#123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";


        public T GetData<T>(string procName, DynamicParameters param, SqlCommandType sqlCommandType = SqlCommandType.StoredProcedure)
        {
            T result = default; //or null 
            using (var con = new SqlConnection(conectionString))
            {
                con.Open();

                if (sqlCommandType == SqlCommandType.StoredProcedure)
                {
                    result = param == null
                        ? con.Query<T>(procName, commandType: CommandType.StoredProcedure).FirstOrDefault()
                        : con.Query<T>(procName, commandType: CommandType.StoredProcedure, param: param).FirstOrDefault();
                }
                else
                {
                    result = param == null
                        ? con.Query<T>(procName).FirstOrDefault()
                        : con.Query<T>(procName, param).FirstOrDefault();
                }
                con.Close();
            }
            return result;
        }

        public async Task<T> GetDataAsync<T>(string procName, DynamicParameters param, SqlCommandType sqlCommandType = SqlCommandType.StoredProcedure)
        {
            T result = default; //or null 
            using (var con = new SqlConnection(conectionString))
            {
                con.Open();

                CommandType? commandType = sqlCommandType == SqlCommandType.StoredProcedure
                    ? CommandType.StoredProcedure
                    : (CommandType?)null;

                var res = await con.QueryAsync<T>(procName, commandType: commandType, param: param);
                result = res.FirstOrDefault();
                con.Close();
            }
            return result;
        }

        public IEnumerable<T> GetDataList<T>(string procName, DynamicParameters param, SqlCommandType sqlCommandType = SqlCommandType.StoredProcedure)
        {
            var commandType = sqlCommandType == SqlCommandType.StoredProcedure
                ? CommandType.StoredProcedure
                : CommandType.Text;

            IEnumerable<T> result = null;
            using (var con = new SqlConnection(conectionString))
            {
                con.Open();
                result = param == null ? con.Query<T>(procName, commandType: commandType) :
                    con.Query<T>(procName, commandType: commandType, param: param);
                con.Close();
            }
            return result;
        }

        public async Task<IEnumerable<T>> GetDataListAsync<T>(string procName, DynamicParameters param, SqlCommandType sqlCommandType = SqlCommandType.StoredProcedure)
        {
            var commandType = sqlCommandType == SqlCommandType.StoredProcedure
               ? CommandType.StoredProcedure
               : CommandType.Text;

            IEnumerable<T> result = null;
            using (var con = new SqlConnection(conectionString))
            {
                con.Open();
                result = param == null ? await con.QueryAsync<T>(procName, commandType: commandType) :
                    await con.QueryAsync<T>(procName, commandType: commandType, param: param);
                con.Close();
            }
            return result;
        }


        public void Execute(string sql, object param, SqlCommandType sqlCommandType = SqlCommandType.StoredProcedure)
        {
            using (var con = new SqlConnection(conectionString))
            {
                con.Open();
                con.Execute(sql, param, commandType: sqlCommandType == SqlCommandType.StoredProcedure ? CommandType.StoredProcedure : CommandType.Text);
                con.Close();
            }
        }

        public async Task ExecuteAsync(string sql, object param, SqlCommandType sqlCommandType = SqlCommandType.StoredProcedure)
        {
            using (var con = new SqlConnection(conectionString))
            {
                con.Open();
                await con.ExecuteAsync(sql, param, commandType: sqlCommandType == SqlCommandType.StoredProcedure ? CommandType.StoredProcedure : CommandType.Text);
                con.Close();
            }
        }

        public async Task<object> GetDataScalarAsync(string procName, DynamicParameters param)
        {
            object result = null;
            using (var con = new SqlConnection(conectionString))
            {
                con.Open();
                result = param == null ? await con.ExecuteScalarAsync(procName, commandType: CommandType.StoredProcedure) :
                    await con.ExecuteScalarAsync(procName, commandType: CommandType.StoredProcedure, param: param);
                con.Close();
            }
            return result;
        }

        public object GetDataScalar(string procName, DynamicParameters param)
        {
            object result = null;
            using (var con = new SqlConnection(conectionString))
            {
                con.Open();
                result = param == null ? con.ExecuteScalar(procName, commandType: CommandType.StoredProcedure) :
                    con.ExecuteScalar(procName, commandType: CommandType.StoredProcedure, param: param);
                con.Close();
            }
            return result;
        }
    }
}
