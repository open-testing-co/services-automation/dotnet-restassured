﻿using System.Collections.Generic;
using System.Threading;
using Dapper;
using DotNetRessAssured.Tests.Infrastructure.Helpers;
using DotNetRessAssured.Tests.Repositories;
using DotNetRessAssured.Tests.Repositories.Contracts.Models;
using Newtonsoft.Json;
using NUnit.Framework;

namespace DotNetRessAssured.Tests
{
	class EmailTracking
	{
		static string url;

		[OneTimeSetUp]
		public static void BeforeAll()
		{
			url = FW.url;
		}

		[SetUp]
		public static void BeforeEach()
		{
			FW.SetLogger();
		}

		[Test]
		public void BaseMessage()
		{
			var resultID = new RA.RestAssured()
					  .Given()
						.Host(url)
						.Uri("/api/Emails/send")
						.Header("Content-Type", "application/json")
						.Body(@"{""subject"": ""Mensaje Prueba""}")
					  .When()
						.Post()
					  .Then().Debug()
						.TestBody("Test Body", x => x.error.errorNumber == 0 && x.error.description == "Success").Assert("Test Body")
						.Retrieve(x => x.transactionId);


			QueryRepositoryBase queryRepositoryBase = new QueryRepositoryBase();
			QueryRepository queryRepository = new QueryRepository();

			var parameters = new DynamicParameters();
			string id = resultID.ToString();
			parameters.Add("Id", id);
			Thread.Sleep(10000);


			List<EventLog> result = (List<EventLog>)queryRepositoryBase.GetDataList<EventLog>(queryRepository.Get("GetDataXId"), parameters, SqlCommandType.Sql);
			foreach (var itemLog in result)
			{
				FW.Log.Info($"{itemLog.Timestamp}  \t {itemLog.Status}  \t {itemLog.Detail}  \t {itemLog.IdResponseProvider}");
			}

			var testName = TestContext.CurrentContext.Test.Name.Replace('(', '_').Replace("\"", "");
			FW.Log.Info($"{resultID} -> {testName}");
			FW.Log.Info("");


			var statuses = new RA.RestAssured()
					  .Given()
						.Host(url)
						.Uri($"/api/Emails/tracking/{resultID}")
						.Header("Content-Type", "application/json")
					  .When()
						.Get()
					  .Then().Debug()
						.TestHeader("Test Header", "content-type", x => x.Contains("json")).Assert("Test Header")
						.TestStatus("Test Status", x => x == 200).Assert("Test Status")
						.TestBody("Test Body", x => x.result.currentStatus == "Successful").Assert("Test Body")
						.Debug()
						.Retrieve(x => x.result.details);


			var objectstatues = JsonConvert.SerializeObject(statuses);
			var lsstatuses = JsonConvert.DeserializeObject<List<TrackingStatus>>(objectstatues);

			Assert.AreEqual(result.Count, lsstatuses.Count);
			for (int i = 0; i < result.Count; i++)
			{
				Assert.AreEqual(result[i].Detail, lsstatuses[result.Count - 1 - i].detail);
				Assert.AreEqual(result[i].Status, lsstatuses[result.Count - 1 - i].status);
			}


		}

		[Test]
		public void SmsTrackingAsEmail()
		{
			var resultID = new RA.RestAssured()
					  .Given()
						.Host(url)
						.Uri("/api/Sms/send")
						.Header("Content-Type", "application/json")
						.Body(@"{""subject"": ""Mensaje Prueba""}")
					  .When()
						.Post()
					  .Then().Debug()
						.TestBody("Test Body", x => x.error.errorNumber == 0 && x.error.description == "Success").Assert("Test Body")
						.Retrieve(x => x.transactionId);


			QueryRepositoryBase queryRepositoryBase = new QueryRepositoryBase();
			QueryRepository queryRepository = new QueryRepository();

			var parameters = new DynamicParameters();
			string id = resultID.ToString();
			parameters.Add("Id", id);
			Thread.Sleep(10000);


			List<EventLog> result = (List<EventLog>)queryRepositoryBase.GetDataList<EventLog>(queryRepository.Get("GetDataXId"), parameters, SqlCommandType.Sql);
			foreach (var itemLog in result)
			{
				FW.Log.Info($"{itemLog.Timestamp}  \t {itemLog.Status}  \t {itemLog.Detail}  \t {itemLog.IdResponseProvider}");
			}

			var testName = TestContext.CurrentContext.Test.Name.Replace('(', '_').Replace("\"", "");
			FW.Log.Info($"{resultID} -> {testName}");
			FW.Log.Info("");


			var statuses = new RA.RestAssured()
					  .Given()
						.Host(url)
						.Uri($"/api/Emails/tracking/{resultID}")
						.Header("Content-Type", "application/json")
					  .When()
						.Get()
					  .Then().Debug()
						.TestHeader("Test Header", "content-type", x => x.Contains("json")).Assert("Test Header")
						.TestStatus("Test Status", x => x == 200).Assert("Test Status")
						.TestBody("Test Body", x => x.error.description == "Code invalid for email tracking").Assert("Test Body")
						.Debug()
						.Retrieve(x => x.result);


			FW.Log.Info($"{statuses}");

		}
	}
}
