﻿using System.IO;

namespace DotNetRessAssured.Tests.Logging
{
    public class SaveAsFile
    {
        private readonly string _filePath;

        public SaveAsFile(string fullpath)
        {
            _filePath = fullpath;
        }

        public void Save(object content)
        {
            File.WriteAllText(_filePath, content.ToString());
        }


    }
}
