﻿using System;
using System.IO;
using DotNetRessAssured.Tests.Logging;
using NUnit.Framework;

namespace DotNetRessAssured.Tests
{
    public class FW
    {
        public static string WORKSPACE_DIRECTORY = Path.GetFullPath(@"../../../../../");
        public static string url = "http://DotNetRessAssured-api-dev.azurewebsites.net";
        //public static string url = "http://localhost:82";

        public static Logger Log => _logger ?? throw new NullReferenceException("_logger is null. set logger first.");
        public static SaveAsFile Save => _saver ?? throw new NullReferenceException("_saver is null. set saver firts.");

        [ThreadStatic] private static Logger _logger;
        [ThreadStatic] private static SaveAsFile _saver;


        public static void SetLogger()
        {
            lock (_setLoggerLock)
            {
                string testDirectoryAcu = CreateTestCaseDirectory();

                String fileName = Path.Combine(testDirectoryAcu, "log.txt");
                if (File.Exists(fileName))
                {
                    var nameString = "log_" + DateTime.Now.ToLocalTime().ToString("yyyyMMddHHmmss") + ".txt";
                    fileName = Path.Combine(testDirectoryAcu, nameString);
                }

                _logger = new Logger(TestContext.CurrentContext.Test.Name, fileName);
            }
        }

        public static void SetSaveAs(string saveFormat)
        {
            lock (_setSaverLock)
            {
                string fullPath = CreateTestCaseDirectory();

                var testName = TestContext.CurrentContext.Test.Name.Replace('(', '_').Replace("\"", "").Replace(")", "");
                fullPath = Path.Combine(fullPath, testName);
                String file;

                if (File.Exists($"{fullPath}.{saveFormat}"))
                {
                    file = fullPath + "_" + DateTime.Now.ToLocalTime().ToString("yyyyMMddHHmmss") + "." + saveFormat;
                }
                else
                {
                    file = $"{fullPath}.{saveFormat}";
                }

                _saver = new SaveAsFile(file);
            }
        }

        private static string CreateTestCaseDirectory()
        {
            var currentFullTest = AddStringBegindToArray("TestResults",
                                TestContext.CurrentContext.Test.FullName.Replace(typeof(FW).Namespace, "").Trim('.').Split('.'));
            var testDirectoryAcu = WORKSPACE_DIRECTORY;

            foreach (var item in currentFullTest)
            {
                var item_ = item.Replace('(', '_').Replace("\"", "").Replace(")", "");
                testDirectoryAcu = Path.Combine(testDirectoryAcu, item_);
                if (!Directory.Exists(testDirectoryAcu))
                {
                    Directory.CreateDirectory(testDirectoryAcu);
                }
            }

            return testDirectoryAcu;
        }

        private static string[] AddStringBegindToArray(String textStart, string[] arrayEnd)
        {
            string[] result = new string[arrayEnd.Length + 1];
            result[0] = textStart;
            arrayEnd.CopyTo(result, 1);
            return result;
        }

        private static readonly object _setLoggerLock = new object();
        private static readonly object _setSaverLock = new object();
    }
}
