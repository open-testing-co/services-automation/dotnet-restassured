﻿using NUnit.Framework;

namespace DotNetRessAssured.Tests
{
    class EmailRender
    {
        static string url;

        [OneTimeSetUp]
        public static void BeforeAll()
        {
            url = FW.url;
        }

        [SetUp]
        public static void BeforeEach()
        {
            FW.SetSaveAs("html");
        }

        [Test]
        public void BaseMessage()
        {
            var content = new RA.RestAssured()
                       .Given()
                         .Host(url)
                         .Uri("/api/Emails/render")
                         .Header("Content-Type", "application/json")
                         .Body(@"{""subject"": ""Mensaje Prueba""}")
                       .When()
                         .Post()
                       .Then().Debug()
                         .TestHeader("Test Header", "content-type", x => x.Contains("json")).Assert("Test Header")
                         .TestStatus("Test Status", x => x == 200).Assert("Test Status")
                         .TestBody("Test Body", x => x.error.errorNumber == 0 && x.error.description == "Success")
                         .Assert("Test Body")
                         .Schema(@"{ ""transactionId"": ""99999eee-00ff-8888-77dd-cccc6666aaaa"", 
                                   
                                    ""error"": {
                                        ""errorNumberXX"": 0,
                                        ""description"": ""Success"",
                                        ""severity"": 0 },
                                    ""responseDateTime"": ""2019-12-31T24:59:59.9999999Z"" }").Assert("Test Schema")
                         .Debug()
                         .Retrieve(x => x.result.content);
            FW.Save.Save(content);
        }

        [Test]
        public void LoadTest()
        {
            var content = new RA.RestAssured()
                       .Given()
                         .Host(url)
                         .Uri("/api/Emails/render")
                         .Header("Content-Type", "application/json")
                         .Body(@"{""subject"": ""Mensaje Prueba""}")
                       .When()
                        //5 threads that runs for 15 seconds
                        .Load(1, 1)
                        .Post()
                        .Debug()
                      .Then().Debug()
                         .TestLoad("good-average", "maximum-ttl-ms", x => x < 100000)
                         .Assert("good-average");
        }


        [TestCase("DirectOnlinePaymentCOL")]
        [TestCase("ExpressChilePaymentInformation")]
        [TestCase("CashPeruPaymentInformationNew")]
        [TestCase("AztecaTemplate")]
        public void Templates(string templateID)
        {
            var content = new RA.RestAssured()
                       .Given()
                         .Host(url)
                         .Uri("/api/Emails/render")
                         .Header("Content-Type", "application/json")
                         .Body(@"{""subject"": "+ templateID + "}").Debug()
                       .When()
                         .Post()
                       .Then().Debug()
                         .TestHeader("Test Header", "content-type", x => x.Contains("json")).Assert("Test Header")
                         .TestStatus("Test Status", x => x == 200).Assert("Test Status")
                         .TestBody("Test Body", x => x.error.errorNumber == 0 && x.error.description == "Success")
                         .Assert("Test Body")
                         .Schema(@"{ ""transactionId"": ""99999eee-00ff-8888-77dd-cccc6666aaaa"", 
                                   
                                    ""error"": {
                                        ""errorNumberXX"": 0,
                                        ""description"": ""Success"",
                                        ""severity"": 0 },
                                    ""responseDateTime"": ""2019-12-31T24:59:59.9999999Z"" }").Assert("Test Schema")
                         .Debug()
                         .Retrieve(x => x.result.content);
            FW.Save.Save(content);
        }
    }
}
