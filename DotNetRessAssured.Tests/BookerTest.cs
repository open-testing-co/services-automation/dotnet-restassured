﻿using NUnit.Framework;

namespace DotNetRessAssured.Tests
{
    class BookerTest
    {
        [Test]
        public void sadas()
        {
            new RA.RestAssured().Given().When().Get().Then();
        }

        [Test]
        public void GetBooking()
        {
            new RA.RestAssured()
                .Given()
                    .Header("Accept", "application/json")
                .When().Get("https://restful-booker.herokuapp.com/booking")
                .Then().TestStatus("Test Status", x => x == 200)
                .TestBody("Test Body", x => x[1] != null)
                .Debug().AssertAll()
                ;
        }

        [Test]
        public void CreateBooking()
        {
            new RA.RestAssured()
                .Given()
                    .Header("Content-Type", "application/json")
                    .Header("Accept", "application/json")
                    .Body(@"{
                        ""firstname"" : ""Henry"",
                        ""lastname"" : ""Correa"",
                        ""totalprice"" : 110000,
                        ""depositpaid"" : true,
                        ""bookingdates"" : {
                            ""checkin"" : ""2019-12-23"",
                            ""checkout"" : ""2020-01-02""
                        },
                        ""additionalneeds"" : ""Breakfast""
                    }")
                .When()
                    .Post("https://restful-booker.herokuapp.com/booking")
                .Then().TestStatus("Test status", x => x == 200)
                .Debug().AssertAll()
                ;
        }

        [Test]
        public void ConsultBookingByID()
        {
            new RA.RestAssured()
                .Given()
                    .Header("Accept", "application/json")
                .When().Get("https://restful-booker.herokuapp.com/booking/12")
                .Then().TestStatus("Test status", x => x == 200)
                .Debug().AssertAll()
                ;
        }

    }
}
