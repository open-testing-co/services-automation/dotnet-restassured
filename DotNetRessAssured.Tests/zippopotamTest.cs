﻿using NUnit.Framework;

namespace DotNetRessAssured.Tests
{
    public class ZippopotamTest
    {
        static string url;

        [OneTimeSetUp]
        public static void BeforeAll()
        {
            url = "http://zippopotam.us";
        }

        [SetUp]
        public static void BeforeEach()
        {
        }

        [Test]
        public void RequestUsZipCode90210_checkPlaceNameInResponseBody_expectBeverlyHills()
        {
            var place = new RA.RestAssured()
                      .Given()
                      .Host(url)
                         .Uri("/us/90210")
                      .When()
                        .Get()
                      .Then()
                        .Retrieve(x => x.places);
            var place1st = new RA.RestAssured()
                      .Given()
                      .When()
                        .Get("http://zippopotam.us/us/90210")
                      .Then()
                        .Retrieve(x => x.places[0]);

            //var aaa = from c in place
            //          select c

            new RA.RestAssured()
                      .Given()
                      .When()
                        .Get("http://zippopotam.us/us/90210")
                      .Then().Debug()
                        .TestHeader("Test Header", "content-type", x => x.Contains("json"))
                        .TestStatus("Test Status", x => x == 200)
                        .TestBody("Test Body", x => x.places[0].state == "California")
                        .AssertAll();
        }

        [Test]
        public void RequestUsZipCode90210_checkStatusCode_expectHttp200()
        {
            new RA.RestAssured()
                .Given()
                .When()
                    .Get("http://zippopotam.us/us/90210")
                .Then().Debug()
                    .TestStatus("Test Status", x => x == 200)
                    .AssertAll();
        }


        [Test]
        public void LoadTest()
        {
            new RA.RestAssured()
            .Given()
                .Name("JsonIP multi thread")
            .When()
                //Configure a load test with
                //6 threads that runs for 30 seconds
                .Load(6, 30)
                //Using this address
                .Get("http://zippopotam.us/us/90210")
            .Then()
                //Assert Load Test and Print Results
                .Debug()
            .TestLoad("good-average", "average-ttl-ms", x => x > 100 && x < 400)
                .Assert("good-average");
        }

        [Test]
        public void RequestUsZipCode90210_checkContentType_expectApplicationJson()
        {
            new RA.RestAssured()
                .Given()
                .When()
                    .Get("http://zippopotam.us/us/90210")
                .Then().Debug()
                    .TestHeader("Test Header", "content-type", x => x.Equals("application/json"))
                    .AssertAll();
        }

        [Test]
        public void RequestUsZipCode90210_logRequestAndResponseDetails()
        {
            new RA.RestAssured()
                .Given()
                .Debug()
            .When()
                    .Get("http://zippopotam.us/us/90210")
            .Then().Debug();
        }


        [TestCase("us", "90210", "California")]
        [TestCase("us", "12345", "New York")]
        [TestCase("ca", "B2R", "Nova Scotia")]
        public void RequestZipCodesFromCollection_checkPlaceNameInResponseBody_expectSpecifiedPlaceName(string countryCode, string zipCode, string expectedPlaceName)
        {
            new RA.RestAssured()
                .Given()
                .Debug()
            .When()
                    .Get("http://zippopotam.us/" + countryCode + "/" + zipCode)
                    .Debug()
            .Then().Debug()
                .TestBody("Test Body", x => x.places[0].state == expectedPlaceName)
                        .AssertAll();
        }


        [Test]
        public void RequestUsZipCode90210_checkPlaceNameInResponseBody_expectBeverlyHills_withRequestSpec()
        {
            new RA.RestAssured()
                .Given()
                .Host("http://zippopotam.us")
                .Uri("/us/90210")
                .Debug()
            .When()
                    .Get()
            .Then().Debug()
                .TestStatus("Test Status", x => x == 200)
                .AssertAll();
        }

        [Test]
        public void TestSchemaValidation()
        {
            new RA.RestAssured()
                .Given()
                    .Header("Content-Type", "application/json")
                    .Header("Accept-Encoding", "gzip, deflate")
                    .Host("https://catalog.data.gov/api/3")
                    .Uri("/api/3")
                //.Uri("/local-partners/marketplace/health/connected")
                .When()
                    .Get()
                    .Debug()
                .Then().Debug()
                    .Schema(@"{""$id"":""http://example.com/example.json"",
                                ""type"":""object"",
                                ""definitions"":{},""$schema"":""http://json-schema.org/draft-07/schema#"",
                                ""properties"":{""version"":{""$id"":""/properties/version"",""type"":""integer"",
                                ""title"":""TheVersionSchema"",""default"":0,""examples"":[3]}}}")
                    .Debug()
                    .AssertSchema();
        }


    }
}
