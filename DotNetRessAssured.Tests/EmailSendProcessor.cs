﻿using Dapper;
using DotNetRessAssured.Tests.Infrastructure.Helpers;
using DotNetRessAssured.Tests.Repositories;
using DotNetRessAssured.Tests.Repositories.Contracts.Models;
using NUnit.Framework;

namespace DotNetRessAssured.Tests
{
    class EmailSendProcessor
    {
        static string url;

        [OneTimeSetUp]
        public static void BeforeAll()
        {
            FW.SetLogger();
            url = FW.url;
        }

        [Test]
        public void AccountIdEmpty()
        {
            var resultID = new RA.RestAssured()
                      .Given()
                        .Host(url)
                        .Uri("/api/Emails/send")
                        .Header("Content-Type", "application/json")
                        .Body(@"{""subject"": ""Mensaje Prueba""}")
                      .When()
                        .Post()
                      .Then().Debug()
                        .TestHeader("Test Header", "content-type", x => x.Contains("json")).Assert("Test Header")
                        .TestStatus("Test Status", x => x == 200).Assert("Test Status")
                        .TestBody("Test Body", x => x.error.errorNumber == 0 && x.error.description == "Success").Assert("Test Body")
                        .Schema(@"{ ""transactionId"": ""99999eee-00ff-8888-77dd-cccc6666aaaa"", 
                                   
                                    ""error"": {
                                        ""errorNumber"": 0,
                                        ""description"": ""Success"",
                                        ""severity"": 0 },
                                    ""responseDateTime"": ""2019-12-31T24:59:59.9999999Z"" }").Assert("Test Schema")
                        .Debug()
                        .Retrieve(x => x.transactionId);

            QueryRepositoryBase queryRepositoryBase = new QueryRepositoryBase();
            QueryRepository queryRepository = new QueryRepository();

            var parameters = new DynamicParameters();
            string id = resultID.ToString();
            parameters.Add("Id", id);

            var result = queryRepositoryBase.GetDataList<EventLog>(queryRepository.Get("GetDataXId"), parameters, SqlCommandType.Sql);
            foreach (var itemLog in result)
            {
                FW.Log.Info($"{itemLog.Timestamp}  \t {itemLog.Status}  \t {itemLog.Detail}  \t {itemLog.IdResponseProvider}");
            }

            var testName = TestContext.CurrentContext.Test.Name.Replace('(', '_').Replace("\"", "");
            FW.Log.Info($"{resultID} -> {testName}");
            FW.Log.Info("");
        }





    }
}
