﻿using Dapper;
using DotNetRessAssured.Tests.Infrastructure.Helpers;
using DotNetRessAssured.Tests.Repositories;
using DotNetRessAssured.Tests.Repositories.Contracts.Models;
using NUnit.Framework;

namespace DotNetRessAssured.Tests
{
    class EmailSend
    {
        static string url;

        [OneTimeSetUp]
        public static void BeforeAll()
        {
            FW.SetLogger();
            url = FW.url;
        }

        [Test]
        public void BaseMessage()
        {
            var resultID = new RA.RestAssured()
                      .Given()
                        .Host(url)
                        .Uri("/api/Emails/send")
                        .Header("Content-Type", "application/json")
                        .Body(@"{""subject"": ""Mensaje Prueba""}")
                      .When()
                        .Post()
                      .Then().Debug()
                        .TestHeader("Test Header", "content-type", x => x.Contains("json")).Assert("Test Header")
                        .TestStatus("Test Status", x => x == 200).Assert("Test Status")
                        .TestBody("Test Body", x => x.error.errorNumber == 0 && x.error.description == "Success").Assert("Test Body")
                        .Schema(@"{ ""transactionId"": ""99999eee-00ff-8888-77dd-cccc6666aaaa"", 
                                   
                                    ""error"": {
                                        ""errorNumber"": 0,
                                        ""description"": ""Success"",
                                        ""severity"": 0 },
                                    ""responseDateTime"": ""2019-12-31T24:59:59.9999999Z"" }").Assert("Test Schema")
                        .Debug()
                        .Retrieve(x => x.transactionId);


            QueryRepositoryBase queryRepositoryBase = new QueryRepositoryBase();
            QueryRepository queryRepository = new QueryRepository();

            var parameters = new DynamicParameters();
            string id = resultID.ToString();
            parameters.Add("Id", id);

            //var result = queryRepositoryBase.GetData<EventLog>(queryRepository.Get("GetDataXId"), parameters, SqlCommandType.Sql);
            //var result = queryRepositoryBase.GetDataList<EventLog>(queryRepository.Get("GetAllData"), null, SqlCommandType.Sql);
            var result = queryRepositoryBase.GetDataList<EventLog>(queryRepository.Get("GetDataXId"), parameters, SqlCommandType.Sql);
            foreach (var itemLog in result)
            {
                FW.Log.Info($"{itemLog.Timestamp}  \t {itemLog.Status}  \t {itemLog.Detail}  \t {itemLog.IdResponseProvider}");
            }

            var testName = TestContext.CurrentContext.Test.Name.Replace('(', '_').Replace("\"", "");
            FW.Log.Info($"{resultID} -> {testName}");
            FW.Log.Info("");

        }

        [Test]
        public void BodyEmpty()
        {
            new RA.RestAssured()
                      .Given()
                        .Host(url)
                        .Uri("/api/Emails/send")
                        .Header("Content-Type", "application/json")
                        .Body("")
                      .When()
                        .Post()
                      .Then().Debug()
                        .TestHeader("Test Header", "content-type", x => x.Contains("json")).Assert("Test Header")
                        .TestStatus("Test Status", x => x == 415).Assert("Test Status")
                        .Debug();
        }

        [Test]
        public void WithoutBody()
        {
            new RA.RestAssured()
                      .Given()
                        .Host(url)
                        .Uri("/api/Emails/send")
                        .Header("Content-Type", "application/json")
                      .When()
                        .Post()
                      .Then().Debug()
                        .TestHeader("Test Header", "content-type", x => x.Contains("json")).Assert("Test Header")
                        .TestStatus("Test Status", x => x == 415).Assert("Test Status")
                        .Debug();
        }

        [Test]
        public void LoadTest()
        {
            new RA.RestAssured()
                      .Given()
                        .Host(url)
                        .Uri("/api/Emails/send")
                        .Header("Content-Type", "application/json")
                        .Body(@"{""subject"": ""Mensaje Prueba""}")
                      .When()
                        //5 threads that runs for 15 seconds
                        .Load(1, 1)
                        .Post()
                        .Debug()
                      .Then().Debug()
                         .TestLoad("good-average", "maximum-ttl-ms", x => x < 100000)
                         .Assert("good-average");
            ;

            /*total-call
              total-succeeded
              total-lost
              average-ttl-ms
              maximum-ttl-ms
              minimum-ttl-ms*/
        }

        [Test]
        public void RecipientsEmailEmpty()
        {
            var resultID = new RA.RestAssured()
                      .Given()
                        .Host(url)
                        .Uri("/api/Emails/send")
                        .Header("Content-Type", "application/json")
                        .Body(@"{""subject"": ""Mensaje Prueba""}")
                      .When()
                        .Post()
                      .Then().Debug()
                        .TestHeader("Test Header", "content-type", x => x.Contains("json")).Assert("Test Header")
                        .TestStatus("Test Status", x => x == 200).Assert("Test Status")
                        .TestBody("Test Body", x => x.error.errorNumber != 0 && x.error.description == "Email recipient is invalid.").Assert("Test Body")
                        .Schema(@"{ ""transactionId"": ""99999eee-00ff-8888-77dd-cccc6666aaaa"", 
                                   
                                    ""error"": {
                                        ""errorNumber"": 0,
                                        ""description"": ""Success"",
                                        ""severity"": 0 },
                                    ""responseDateTime"": ""2019-12-31T24:59:59.9999999Z"" }").Assert("Test Schema")
                        .Debug()
                        .Retrieve(x => x.transactionId);

            QueryRepositoryBase queryRepositoryBase = new QueryRepositoryBase();
            QueryRepository queryRepository = new QueryRepository();

            var parameters = new DynamicParameters();
            string id = resultID.ToString();
            parameters.Add("Id", id);

            var result = queryRepositoryBase.GetDataList<EventLog>(queryRepository.Get("GetDataXId"), parameters, SqlCommandType.Sql);
            foreach (var itemLog in result)
            {
                FW.Log.Info($"{itemLog.Timestamp}  \t {itemLog.Status}  \t {itemLog.Detail}  \t {itemLog.IdResponseProvider}");
            }

            var testName = TestContext.CurrentContext.Test.Name.Replace('(', '_').Replace("\"", "");
            FW.Log.Info($"{resultID} -> {testName}");
            FW.Log.Info("");

        }



    }
}
